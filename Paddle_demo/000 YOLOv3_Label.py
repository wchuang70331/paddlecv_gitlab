import os
import random
import shutil
import cv2
import numpy as np
import xml.etree.ElementTree as ET
from PIL import Image, ImageEnhance

'''
注意事项：
1.路径
2.图像后缀
3.标签名
'''

#类别名称
INSECT_NAMES = ['Boerner', 'Leconte', 'Linnaeus', 
                'acuminatus', 'armandi', 'coleoptera', 'linnaeus']
#########################################################读取信息####################################################
'''
数据结构方式:train
                images
                annotations
                    xmls
其中xmls文件夹下放置标注文件
'''


#获取类别名并与数组下标对应（下标从0开始）
def get_insect_names():
    insect_category2id = {}
    for i, item in enumerate(INSECT_NAMES):
        insect_category2id[item] = i
    return insect_category2id

#获取标注文件信息
def get_annotations(cname2cid, datadir):
    filenames = os.listdir(os.path.join(datadir, 'annotations', 'xmls'))
    records = []

    #图像路径 annotation路径
    imagesPath=[]
    annotationsPath=[]

    ct = 0
    for fname in filenames:
        fid = fname.split('.')[0]
        fpath = os.path.join(datadir, 'annotations', 'xmls', fname)
        #写入标注信息
        annotationsPath.append(fpath)

        img_file = os.path.join(datadir, 'images', fid + '.jpeg')
        tree = ET.parse(fpath)

        if tree.find('id') is None:
            im_id = np.array([ct])
        else:
            im_id = np.array([int(tree.find('id').text)])

        objs = tree.findall('object')
        im_w = float(tree.find('size').find('width').text)
        im_h = float(tree.find('size').find('height').text)
        gt_bbox = np.zeros((len(objs), 4), dtype=np.float32)
        gt_class = np.zeros((len(objs), ), dtype=np.int32)
        is_crowd = np.zeros((len(objs), ), dtype=np.int32)
        difficult = np.zeros((len(objs), ), dtype=np.int32)
        for i, obj in enumerate(objs):
            cname = obj.find('name').text
            gt_class[i] = cname2cid[cname]
            _difficult = int(obj.find('difficult').text)
            x1 = float(obj.find('bndbox').find('xmin').text)
            y1 = float(obj.find('bndbox').find('ymin').text)
            x2 = float(obj.find('bndbox').find('xmax').text)
            y2 = float(obj.find('bndbox').find('ymax').text)
            x1 = max(0, x1)
            y1 = max(0, y1)
            x2 = min(im_w - 1, x2)
            y2 = min(im_h - 1, y2)
            # 这里使用xywh格式来表示目标物体真实框
            gt_bbox[i] = [(x1+x2)/2.0 , (y1+y2)/2.0, x2-x1+1., y2-y1+1.]
            is_crowd[i] = 0
            difficult[i] = _difficult

        voc_rec = {
            'im_file': img_file,
            'im_id': im_id,
            'h': im_h,
            'w': im_w,
            'is_crowd': is_crowd,
            'gt_class': gt_class,
            'gt_bbox': gt_bbox,
            'gt_poly': [],
            'difficult': difficult
            }
        if len(objs) != 0:
            records.append(voc_rec)
            imagesPath.append(img_file)

        ct += 1
    return imagesPath,annotationsPath 

#写入TXT文件
def txtWrite():
    #标注信息路径(这里的路径有所特殊，可根据实际情况进行更改)
    TRAINDIR = 'D:\\Code\\PaddleDetection\\myTest\\YOLOv3_data\\insects\\train'
    cname2cid = get_insect_names()
    imagesPath,annotationsPath  = get_annotations(cname2cid, TRAINDIR)

    imgLen=len(imagesPath)
    annoLen=len(annotationsPath)

    if(imgLen==annoLen):
        #创建txt文件
        txtPath='D:\\Code\\PaddleDetection\\myTest\\YOLOv3_data\\insects\\train.txt'
        # 如果filename不存在会自动创建， 'w'表示写数据，写之前会清空文件中的原有数据！
        # 'a'表示append,即在原来文件内容后继续写数据（不清除原有数据）
        with open(txtPath,'w') as f:
            for i in range(imgLen):
                f.write(imagesPath[i]+'  '+annotationsPath[i]+'\n')
        print("create ok")
    else:
        print("Data Error")
   

if __name__ == '__main__':
    txtWrite()