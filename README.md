SET CUDA_VISIBLE_DEVICES=0


conda info -e
conda activate wang_Paddle


一、Paddle_Detection简单实用

1.1 入门

#训练（建议用绝对路径），另外根据具体情况修改.yml参数
python tools/train.py -c configs/sleeper_yolov3.yml
# 评估
python tools/eval.py -c configs/sleeper_yolov3.yml
# 推断
python tools/infer.py -c configs/sleeper_yolov3.yml --infer_img=demo/000000570688.jpg


